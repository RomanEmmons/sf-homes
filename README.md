# SF Homes

[This simple full-stack application](http://54.183.53.206:3000/) takes in a list of homes for sale in `.csv` format from [Redfin](https://www.redfin.com/city/17151/CA/San-Francisco), then parses and stores the data. Simply click or drag and drop the `.csv` file into the designated area on the screen to upload. There is a count of how many houses match the current search criteria. This count autorefreshes upon successful upload. The application also allows the user to input a search term that returns any homes in the database that contain the term in the street address field. On load the application displays a complete list of all the homes in the database.


## Stack

- React
- Node
- Express
- MySQL


## Deployment

- AWS EC2
- AWS RDS


## Start

- `npm start` (you must run `npm run react:dev` at least one time prior in order to compile bundle before this will work)


## Dev Start

- `npm i` to install dependancies
- `npm run react:dev` triggers Webpack to compile bundle and hot reloading
- `npm run server:dev` starts Express server listening at port 3000 and hot reloading
- `.env` file containing DB credentials inside `/database` is required to start


## TODO

- Better CSS
- TS conversion
- Unit Testing
- HTTPS/SSL
- Pagination
- More robust Parsing class (If a row in the `.csv` contains a value with additional commas, it is omitted from database storage)
- Dropdown containing a list of database columns that allows to user to apply search to fields other than street address
- Upsert on duplicate unique index


