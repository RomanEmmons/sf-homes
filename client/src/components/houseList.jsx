import React from 'react';
import ListItem from './listItem.jsx';

const HouseList = (props) => (
  <div>
    <h3>{props.houses.length} houses match your search.</h3>
    <ul>
      {props.houses.map((house, i) => {
        return <ListItem key={i} value={house} />;
      })}
    </ul>
  </div>
);

export default HouseList;
