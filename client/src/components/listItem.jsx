import React from 'react';
import PropTypes from 'prop-types';

function ListItem(props) {
  let house = props.value;

  const houseData = [
    ['Sale Type', house.sale_type],
    ['Property Type', house.property_type],
    ['Street Address', house.address],
    ['City', house.city],
    ['State', house.state],
    ['Zip', house.zip],
    ['Price', house.price],
    ['Beds', house.beds],
    ['Baths', house.baths],
    ['Location', house.location],
    ['Square Feet', house.square_feet],
    ['Cost Per Square Foot', house.cost_per_square_foot],
    ['Lot Size', house.lot_size],
    ['Year Built', house.year_built],
    ['Days on Market', house.days_on_market],
    ['HOA Cost Per Month', house.hoa_month],
    ['Status', house.status],
    ['Next Open House Start Time', house.next_open_house_start_time],
    ['Next Open House End Time', house.next_open_house_end_time],
    ['Source', house.source],
    ['MLS Number', house.mls_number],
    ['Longitude', house.longitude],
    ['Latitude', house.latitude]
  ]

  return (
    <div>
      <li>
        <h3>
          <a href={house.url}> {house.url} </a>
        </h3>
        {houseData.map((tuple) => <><a>{tuple[0]}: </a><span>{tuple[1]} </span><br></br></>)}
      </li>
    </div>
  )
}
ListItem.propTypes = {
  repos: PropTypes.array,
};

export default ListItem;
